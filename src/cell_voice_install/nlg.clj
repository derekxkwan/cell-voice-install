(ns cvi.nlg
  (:require [clojure.string :as str])
  (:import [simplenlg.framework PhraseElement NLGElement NLGFactory]
           [simplenlg.lexicon Lexicon]
           [simplenlg.realiser.english Realiser]
           [simplenlg.phrasespec PhraseSpec]
           [simplenlg.features Feature NumberAgreement Tense InterrogativeType])
  )
