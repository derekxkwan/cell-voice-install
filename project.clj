(defproject cell-voice-install "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [compojure "1.6.1"]
                 [http-kit "2.3.0"]
                 [ring/ring-core "1.7.0-RC1"]
                 [overtone/osc-clj "0.7.1"]
                 [uk.ac.abdn/SimpleNLG "4.4.8"]]

  :main cell-voice-install.core)
